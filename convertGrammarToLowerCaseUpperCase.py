#! /usr/bin/env python3

import shlex
import sys

def convertToLowerCaseOrUpperCase(filePath):
	with open(filePath) as currentFile:
		for currentLine in currentFile:
			if currentLine.startswith("STRING_LITERAL"):
				print(currentLine)
				continue
			if currentLine.startswith("ECHAR"):
				print(currentLine)
				continue
			wordsList = []
			for currentWord in shlex.split(currentLine, posix=False):
				if currentWord.startswith("'") and currentWord.endswith("'") and currentWord[1:-1].replace(" ", "").replace("_", "").isalpha():
					if currentWord == "'a'":
						wordsList.append("'a'")
					else:
						wordsList.append("( '" + currentWord[1:-1].lower() + "' | '" + currentWord[1:-1].upper() + "' )")
				elif currentWord.startswith("'") and currentWord.endswith("'?") and currentWord[1:-2].isalpha():
					wordsList.append("( '" + currentWord[1:-2].lower() + "' | '" + currentWord[1:-2].upper() + "' )?")
				elif currentWord.startswith("'") and currentWord.endswith("'*") and currentWord[1:-2].isalpha():
					wordsList.append("( '" + currentWord[1:-2].lower() + "' | '" + currentWord[1:-2].upper() + "' )*")
				elif currentWord.startswith("'") and currentWord.endswith("'+") and currentWord[1:-2].isalpha():
					wordsList.append("( '" + currentWord[1:-2].lower() + "' | '" + currentWord[1:-2].upper() + "' )+")
				else:
					wordsList.append(currentWord)
			print(" ".join(wordsList))
			


if __name__ == '__main__':
	if len(sys.argv) != 2:
		print("Usage: {} <pathToGrammarFile>".format(sys.argv[0]))
		sys.exit(1)
	convertToLowerCaseOrUpperCase(sys.argv[1])
