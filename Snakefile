
# snakemake --dag | dot -Tpng -o workflow.png
# snakemake --rulegraph | dot -Tpng -o workflowRules.png


rule all:
	#input: 'Sparql.g4'
	input: 
		'Sparql.g4', 
		'parser-python/Sparql.interp', 
		'parser-python/SparqlLexer.interp', 
		'parser-python/SparqlLexer.py', 
		'parser-python/SparqlLexer.tokens', 
		'parser-python/SparqlListener.py', 
		'parser-python/SparqlParser.py', 
		'parser-python/Sparql.tokens', 
		'parser-python/SparqlVisitor.py'

rule fixParentheses:
	input: 'Sparql-original-modified.g4'
	output: 'Sparql-fixedParentheses.g4'
	shell: "./convertGrammarFixParentheses.py {input} > {output}"

rule generateLowerCaseUpperCase:
	input: 'Sparql-fixedParentheses.g4'
	output: 'Sparql.g4'
	shell: "./convertGrammarToLowerCaseUpperCase.py {input} > {output}"

rule generateParserPython:
	input: 'Sparql.g4'
	output: 
		'parser-python/Sparql.interp', 
		'parser-python/SparqlLexer.interp', 
		'parser-python/SparqlLexer.py', 
		'parser-python/SparqlLexer.tokens', 
		'parser-python/SparqlListener.py', 
		'parser-python/SparqlParser.py', 
		'parser-python/Sparql.tokens', 
		'parser-python/SparqlVisitor.py'
	shell: "java -cp .:/usr/local/lib/antlr-4.10.1-complete.jar -jar /usr/local/lib/antlr-4.10.1-complete.jar -visitor -Dlanguage=Python3 -o parser-python/ {input}"


rule generateWorkflowGraph:
	input: 'Snakefile'
	output: 'workflow.png'
	#message: "Generating graph..."
	shell: 'snakemake --dag | dot -Tpng -o {output}'
	#shell: 'snakemake --forceall --rulegraph | dot -Tpng -o {output}'

