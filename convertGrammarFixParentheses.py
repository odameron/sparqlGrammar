#! /usr/bin/env python3

import sys

def fixParentheses(filePath):
	with open(filePath) as currentFile:
		for currentLine in currentFile:
			wordsList = []
			wordIndex = 0
			for currentWord in currentLine.split():
				#if currentWord.startswith("(") and (len(currentWord) > 1):
					#print("case1 " + currentWord)
					#wordsList.append("(")
				if currentWord.startswith("//"):
					wordsList.append(" ".join(currentLine.split()[wordIndex:]))
					break
				wordStart = 0
				while (wordStart < len(currentWord)) and (currentWord[wordStart] == "("):
					wordStart += 1
					wordsList.append("(")
				wordClosingSymbols = []
				wordEnd = len(currentWord)
				while (wordEnd > 0) and (currentWord[wordStart:wordEnd].endswith(")") or currentWord[wordStart:wordEnd].endswith("|") or currentWord[wordStart:wordEnd].endswith(";") or currentWord[wordStart:wordEnd].endswith(")+") or currentWord[wordStart:wordEnd].endswith(")*") or currentWord[wordStart:wordEnd].endswith(")?")):
					for endSymbol in [")", "|", ";", ")+", ")*", ")?"]:
						if currentWord[wordStart:wordEnd].endswith(endSymbol):
							wordEnd -= len(endSymbol)
							wordClosingSymbols.insert(0, endSymbol)
							break
				if wordStart < wordEnd:
					wordsList.append(currentWord[wordStart:wordEnd])
				wordsList.extend(wordClosingSymbols)
				wordIndex += 1
			print(" ".join(wordsList))
			


if __name__ == '__main__':
	if len(sys.argv) != 2:
		print("Usage: {} <pathToGrammarFile>".format(sys.argv[0]))
		sys.exit(1)
	fixParentheses(sys.argv[1])
