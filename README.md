# SparqlGrammar


[ANTLR](https://www.antlr.org/)-compatible SPARQL grammar.

This is based on the grammar retrieved from retrieved from https://github.com/tonicebrian/sparql-parser/tree/master/src/main/antlr4 with the following modifications:

- the original `Sparql.g4` was renamed as `Sparql-original.g4`
- `Sparql-original-modified.g4` was created by performing the following modifications:
    - [x] replaced the `@parser::members {public static boolean allowsBlankNodes = true;}` statement (line 3) that conflicts withe the generation of python code by `@parser::members {allowsBlankNodes = True}`
	- [x] replaced the java-like booleans `true;` and `false;` by their python equivalent in the `@init` and `@after` blocks
    - [x] rename `'object'` and `'filter'` to `'objectClause'` and `'filterClause'` as they conflict with python keyword in the generated code
    - [x] remove the uppercase alternative for `'a'`
- according to the SPARQL specification (note 1 section 19.8 https://www.w3.org/TR/sparql11-query/#grammar), "Keywords are matched in a case-insensitive manner with the exception of the keyword 'a'"
    - the original grammar only supports uppercase keywords (except for 'a', surprisingly)
    - [x] **DONE:** add lowercase versions of the keywords to the grammar
        - whole process automated with `snakemake`
        - run `./convertGrammarFixParentheses.py Sparql-original-modified.g4 > Sparql-fixedParentheses.g4`
        - run `./convertGrammarToLowerCaseUpperCase.py Sparql-fixedParentheses.g4 > Sparql.g4`
- Alix Régnier noticed that the original grammar is not compliant with the W3C SPARQL specification at https://www.w3.org/TR/sparql11-query/#grammar (`IRIREF` rule).
	- [ ] **TODO:** fix `IRIREF`


# Installation 
- cf https://www.antlr.org/
	- `$ cd /usr/local/lib`
	- `$ sudo curl -O https://www.antlr.org/download/antlr-4.10.1-complete.jar`
	- `$ export CLASSPATH=".:/usr/local/lib/antlr-4.10.1-complete.jar:$CLASSPATH"`
	- `$ alias antlr4='java -jar /usr/local/lib/antlr-4.10.1-complete.jar'`
	- `$ alias grun='java org.antlr.v4.gui.TestRig'`
- `sudo pip3 install antlr4-python3-runtime`

```bash
antlr4 -visitor -Dlanguage=Python3 Sparql.g4
python3 SparqlAnalyzer.py testQuery.rq
```

# visualisation of the parse tree

cf `showGrammar.bash`:

```bash
#! /usr/bin/env bash

antlr4JarPath="/usr/local/lib/antlr-4.10.1-complete.jar"

export CLASSPATH=".:${antlr4JarPath}:$CLASSPATH"
antlr4="java -jar ${antlr4JarPath}"
grun='java org.antlr.v4.gui.TestRig'

$antlr4 Sparql.g4
javac Sparql*.java
#$grun Sparql statement -gui ../testQuery.rq
#$grun Sparql statement -gui ../testQuery-alternateProperty.rq
#$grun Sparql statement -gui ../testQuery-sequenceProperty.rq
#$grun Sparql statement -gui ../testQuery-sameSubject.rq
#$grun Sparql statement -gui ../testQuery-sameSubjectSameProperty.rq
#$grun Sparql statement -gui ../testQuery-sameSubjectPropertyAlternativePropertySequence.rq
#$grun Sparql statement -gui ../testQuery-datatypeValue.rq
#$grun Sparql statement -gui ../biopax-invalid-complexes.rq
#$grun Sparql statement -gui ../query-redundancy-heterodimeres-getRedundantCurationEvents.rq
$grun Sparql statement -gui ../testQuery-filterNotExists.rq
```


Resources:
- https://faun.pub/introduction-to-antlr-python-af8a3c603d23
- https://jayconrod.com/posts/37/a-simple-interpreter-from-scratch-in-python-part-1


# Resources
- https://faun.pub/introduction-to-antlr-python-af8a3c603d23
- https://jayconrod.com/posts/37/a-simple-interpreter-from-scratch-in-python-part-1


# Similar works:
- https://semsys.ifs.tuwien.ac.at/visualizing-sparql-graph-pattern/
- https://gist.github.com/pebbie/5aafdbb14921cadbad53011ef5f61165
    - reviews mention http://aidanhogan.com/docs/rdf_explorer_demo.pdf




# TODO

- [x] add snakefile
